#pragma once
/*
* pipe is a module for server connection each other.
*
* Copyright (C) 2021-2023 gavingqf(gavingqf@126.com)
*
*    Distributed under the Boost Software License, Version 1.0.
*    (See accompanying file LICENSE_1_0.txt or copy at
*    http://www.boost.org/LICENSE_1_0.txt)
*/
#include <string>
#include <atomic>
#include <tuple>
#include <vector>
#include <sstream>
#include <iostream>
#include <string.h>
/*
 * notes that all pipe's all functions run in the same thread.
 * 管道对外的用户接口，所有接口函数在一个线程执行的。
 */

namespace anet {
	namespace pipe {
		class IPipeMsgHandler;
		class IPipeReporter;

		// server type, where you can add more here.
    #define GameType   0
    #define GateType   1
    #define DBType     2
    #define LoginType  3
    #define CenterType 4

		// convert string to uint32.
		inline unsigned int convertToUint32(const std::string& strPipeId) {
			std::vector<unsigned char> values;
			std::stringstream ss(strPipeId);
			std::string token;

			// 分割字符串并转换每个部分为unsigned char
			while (std::getline(ss, token, '-')) {
				int value = std::stoi(token);
				if (value < 0 || value > 255) {
					throw std::out_of_range("Value out of range (0-255)");
				}
				values.push_back(static_cast<unsigned char>(value));
			}

			// 确保我们有4个值
			if (values.size() != 4) {
				throw std::invalid_argument("Input string must contain exactly 4 values");
			}

			// 整合成一个uint32.
			unsigned int result = 0;
			result |= static_cast<unsigned int>(values[0]) << 24;
			result |= static_cast<unsigned int>(values[1]) << 16;
			result |= static_cast<unsigned int>(values[2]) << 8;
			result |= static_cast<unsigned int>(values[3]);

			return result;
		}

		// get all char from uint32.
		inline std::tuple<unsigned char, unsigned char, unsigned char, unsigned char>
			extractPipe(unsigned int pipeId) {
			unsigned char area = (pipeId >> 24) & 0xFF;
			unsigned char group = (pipeId >> 16) & 0xFF;
			unsigned char type = (pipeId >> 8) & 0xFF;
			unsigned char index = pipeId & 0xFF;
			return std::make_tuple(area, group, type, index);
		}

		// transfer pipe id to string.
		inline std::string getPipeIdStr(unsigned int pipeId) {
			auto tuple = extractPipe(pipeId);
			char buff[1024] = { 0 };
			auto area = std::get<0>(tuple);
			auto group = std::get<1>(tuple);
			auto type = std::get<2>(tuple);
			auto index = std::get<3>(tuple);
			std::snprintf(buff, sizeof(buff)-1, "%d-%d-%d-%d", area, group, type, index);
			return buff;
		}

		// pipe log interface.
		class IPipeLog {
		public:
			virtual ~IPipeLog() {}

			// log interface.
			virtual bool setLevel(int level) = 0;
			virtual int  getLevel() const = 0;
			virtual void Debug(const char* format, ...) = 0;
			virtual void Info(const char* format, ...) = 0;
			virtual void Warn(const char* format, ...) = 0;
			virtual void Crit(const char* format, ...) = 0;
		};

		// global log variable.
		extern IPipeLog* gLog;
		inline void SetLog(IPipeLog* log) {
			if (log != nullptr) {
				gLog = log;
			}
		}

		// open flag.
		extern std::atomic_bool pipeLogOpen;
		inline void SetLogFlag(bool open) {
			pipeLogOpen = open;
		}

		// pipe module interface.
		class IPipeModule {
		public:
			virtual ~IPipeModule() {}

			// initialize pipe module with xml config and pipe reporter callback.
			virtual bool Init(const char* pipePath, IPipeReporter* reporter) = 0;

			// run pipe module(tcp && timer && service_discovery).
			virtual bool Run(int count) = 0;

			// get reporter.
			virtual IPipeReporter* GetReporter() = 0;

			// local pipe id.
			virtual unsigned int GetLocalId() const = 0;

			// local pipe name.
			virtual const std::string& GetLocalName() const = 0;
		};

		// pipe interface.
		class IPipe {
		public:
			virtual ~IPipe() {}

			// get remote pipe id(area-group-type-index)
			virtual unsigned int GetRemotePipeId() const = 0;
			// get remote pipe name(as game, db, gate)
			virtual const std::string& GetRemotePipeName() const = 0;

			// set pipe message handler.
			virtual void SetMsgHandler(IPipeMsgHandler* msgHandler) = 0;

			// get pipe message handler.
			virtual IPipeMsgHandler* GetMsgHandler() = 0;

			// send pipe message(need no packet header).
			virtual void Send(const char* msg, int len) = 0;

			// close pipe
			virtual void Close() = 0;
		};

		// pipe status reporter callback interface.
		class IPipeReporter {
		public:
			virtual ~IPipeReporter() {}

			// pipe is connected or so callback.
			// user must call IPipe's SetMsgHandler interface to set its message handler.
			// isConnected == true denotes connected and else denotes terminate
			// if it is connected, the pipe is valid,
			// and if it is terminated, the pipe is invalid now.
			virtual void OnReport(bool isConnected, IPipe* pipe) = 0;
		};

		// pipe message handler callback interface.
		class IPipeMsgHandler {
		public:
			virtual ~IPipeMsgHandler() {}

			// pipe receives message callback.
			// !!The msg has no tcp header information(just the user sending message).
			virtual void Handle(const char* msg, int len) = 0;
		};

		// global function to get pipe module.
		IPipeModule* GetPipeModule();
	}
}