#pragma once
/*
 * pipe message define.
 */
#include "pipe_define.h"

namespace anet {
	namespace pipe {
		// pipe message id.
		enum ePipeMessageId : uint16 {
			pipe_message_beat_heart_id  = uint16((0x1 << 16) - 1),
			pipe_message_connect_req_id = uint16((0x1 << 16) - 2),
			pipe_message_connect_ack_id = uint16((0x1 << 16) - 3),
		};

		// pipe message struct.
    #pragma pack(push, 1)
		// connect require struct.
		struct connect_req {
			// remote pipe id.
			uint32 remote_id;

			// remote pipe name.
			char   remote_name[32];

			// connection token.
			char   token[32];
		};

		// connect ack struct.
		struct connect_ack {
		};

		// beat heart
		struct heart_beat {
		};
    #pragma pack(pop)
	}
}
