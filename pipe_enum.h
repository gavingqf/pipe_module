#pragma once
/*
 * pipe enum as pipe mode and pipe state.
 */

namespace anet {
    namespace pipe {
        // pipe mode
        enum class ePipeMode : int {
            // invalid mode
            pipe_mode_invalid = -1,

            // connector mode
            pipe_mode_connector = 0,

            // listen mode.
            pipe_mode_listener = 1,
        };

        // pipe state
        enum class ePipeState : int {
            // invalid pipe state
            pipe_session_state_invalid = -1,

            // tcp net connected state
			pipe_session_state_net_connected = 0,

            // pipe disconnected state              
			pipe_session_state_pipe_disconnected = 1,  

            // pipe connected state.          
			pipe_session_state_pipe_connected = 2,
        }; 
    }
}