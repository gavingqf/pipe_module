#include "pipe_session.h"
#include "pipe_protocol.h"
#include "pipe_module.h"
#include "client.hpp"
#include "time.hpp"

namespace anet {
	namespace pipe {
		// safe copy.
		template <size_t N>
		static void safeCopy(char (&left)[N], const std::string &right) {
			auto minSize = right.size() >= N ? N - 1 : right.size();
			strncpy(left, right.c_str(), minSize);
			left[minSize] = 0;
		}

		CPipeSession::CPipeSession() : m_lastBeatHeart(GetNowMSTime()),
			m_remoteId(0), m_remoteName(""), m_token(""), m_session(nullptr),
			m_mode(ePipeMode::pipe_mode_listener), // default is listener.
			m_state(ePipeState::pipe_session_state_invalid),
			m_pipeHandler(nullptr), m_client(nullptr), 
			m_timer(CPipeModule::instance().GetTimeWheel()) {
		}

		CPipeSession::~CPipeSession() {
			if (m_mode == ePipeMode::pipe_mode_connector) {
				assert(m_client != nullptr && "client pointer is nullptr");
				m_client = nullptr;
			}
			m_pipeHandler = nullptr;
			m_session = nullptr;
		}

		void CPipeSession::SetRemoteId(uint32 id) {
			m_remoteId = id;
		}

		void CPipeSession::SetRemoteName(const std::string& name) {
			m_remoteName = name;
		}

		void CPipeSession::SetToken(const std::string& token) {
			m_token = token;
		}

		void CPipeSession::SetMode(ePipeMode mode) {
			m_mode = mode;
		}

		void CPipeSession::SetMsgHandler(IPipeMsgHandler* pipeHandler) {
			assert(pipeHandler != nullptr && "msg handler is nullptr");
			if (pipeHandler == nullptr) {
				gLog->Crit("user sets a nullptr for pipe handler.");
				return;
			}
			m_pipeHandler = pipeHandler;
		}

		uint32 CPipeSession::GetRemotePipeId() const {
			return m_remoteId;
		}

		const std::string& CPipeSession::GetRemotePipeName() const {
			return m_remoteName;
		}

		IPipeMsgHandler* CPipeSession::GetMsgHandler() {
			return m_pipeHandler;
		}

		void CPipeSession::Close() {
			m_session->Close();
		}

		void CPipeSession::Send(const char* msg, int len) {
			if (msg == nullptr || len == 0) {
				return;
			}

			if (m_state != ePipeState::pipe_session_state_pipe_connected) {
				return;
			}
			
			// build message.
			std::vector<char> buffer;
			buffer.resize(sizeof(anet::tcp::SCommonHead) + len);

			char* pBuff = &buffer[0];
			anet::tcp::SCommonHead* pHead = (anet::tcp::SCommonHead*)pBuff;
			pHead->len = (decltype(pHead->len))htonl(len);

			memcpy(pBuff + sizeof(anet::tcp::SCommonHead), msg, len);
			m_session->Send(pBuff, int(sizeof(anet::tcp::SCommonHead) + len));
		}

		void CPipeSession::SetClient(tcpClientPtr client) {
			assert(m_mode == ePipeMode::pipe_mode_connector && 
				client != nullptr &&
				"only connector mode can set client"
			);
			m_client = client;
		}

		CPipeSession* CPipeSession::Create() {
			return new CPipeSession();
		}

		void CPipeSession::SetSession(sharePtrSession* pSession) {
			m_session = pSession;
		}

		void CPipeSession::OnMessage(const char* msg, int len) {
			if (len < int(sizeof(anet::tcp::SCommonHead))) {
				return;
			}

			// set beat heart time(all the message are as beat heart message).
			m_lastBeatHeart = GetNowMSTime();

			switch (m_state) {
			case ePipeState::pipe_session_state_net_connected: {
				if (len < int(sizeof(anet::tcp::SCommonHead) + sizeof(uint16))) {
					gLog->Crit("%s(%d) find an invalid packet", this->GetRemotePipeName().c_str(), this->GetRemotePipeId());
					return ;
				}

				// message id.
				uint16 msgId = ntohs(*(uint16*)(msg + sizeof(anet::tcp::SCommonHead)));

				if (m_mode == ePipeMode::pipe_mode_listener) {
					// listen peer.
					if (msgId == ePipeMessageId::pipe_message_connect_req_id) {
						// connect_req message.
						connect_req* req = (connect_req*)(msg + sizeof(anet::tcp::SCommonHead) + sizeof(uint16));
						if (!CPipeModule::instance().CheckConnectInfo(req->remote_name, req->token)) {
							gLog->Crit("remote name(%s),id:%d,token:%s is not the same with local information", 
								req->remote_name, req->remote_id, req->token);
							m_session->Close();
							return;
						}

						m_state = ePipeState::pipe_session_state_pipe_connected;
						m_remoteId = req->remote_id;
						m_remoteName = req->remote_name;

						// send ack message.
						this->sendConnectAck();

						// call pipe connected reporter.
						CPipeModule::instance().GetReporter()->OnReport(true, this);

						// start timer to check beat heat.
						this->startToCheckBeatHeart();
					} else if (msgId == ePipeMessageId::pipe_message_beat_heart_id) {
						// beat heart message.
					} else {
						gLog->Crit("%d find an invalid message id:%d", this->GetRemotePipeId(), msgId);
						m_state = ePipeState::pipe_session_state_invalid;
						m_session->Close();
					}
				} else if (m_mode == ePipeMode::pipe_mode_connector) {
					// connector peer.
					if (msgId == ePipeMessageId::pipe_message_connect_ack_id) {
						m_state = ePipeState::pipe_session_state_pipe_connected;

						// call pipe connected reporter.
						CPipeModule::instance().GetReporter()->OnReport(true, this);

						// start to send beat heart.
						this->startToSendBeatHeart();
					} else {
						gLog->Crit("%d find an invalid message id:%d", GetRemotePipeId(), msgId);
						m_state = ePipeState::pipe_session_state_invalid;
						m_session->Close();
					}
				} else {
					gLog->Crit("%d find invalid state:%d", GetRemotePipeId(), int(m_state));
					m_session->Close();
					return;
				}
			}
			break;

			case ePipeState::pipe_session_state_pipe_connected: {
				if (len == sizeof(anet::tcp::SCommonHead) + sizeof(uint16) + sizeof(heart_beat)) {
					// beat heart message check.
					uint16 msgId = ntohs(*(uint16*)(msg + sizeof(anet::tcp::SCommonHead)));
					if (msgId == ePipeMessageId::pipe_message_beat_heart_id) {
						// excluding pipe message's beat heart id.
						return;
					}
				}

				if (m_pipeHandler != nullptr) {
					// remove the protocol header.
					auto headSize = sizeof(anet::tcp::SCommonHead);
					m_pipeHandler->Handle(msg + headSize, len - headSize);
				} else {
					gLog->Crit("can not find %d pipe's message handler", this->GetRemotePipeId());
				}
			}
			break;

			case ePipeState::pipe_session_state_invalid: {
				return;
			}
			break;

			case ePipeState::pipe_session_state_pipe_disconnected: {
				return;
			}
			break;
		  }
		}

		void CPipeSession::startToSendBeatHeart() {
			m_timer.add_repeated_timer([this](void* pData) {
				(void)pData; // remove warning.
				if (m_state == ePipeState::pipe_session_state_pipe_connected) {
					this->sendBeatHeart();
				} else {
					m_timer.kill_timer(uint64(eConst::pipe_beat_heart_send_timer_id));
				}
			}, uint64(eConst::pipe_beat_heart_send_timer_id),
			uint64(eConst::pipe_beat_heart_send_timer_time));
		}

		void CPipeSession::startToCheckBeatHeart() {
			m_lastBeatHeart = GetNowMSTime();

			// add timer to check beat heart.
			m_timer.add_repeated_timer([this](void* pData) {
				(void)pData; // remove warning.
				if (GetNowMSTime() - m_lastBeatHeart > gBeatHearTime) {
					gLog->Crit("%s(%d) has no beat heart now, just disconnect it.", m_remoteName.c_str(), m_remoteId);
					m_session->Close();
					m_timer.kill_timer(uint64(eConst::pipe_beat_heart_check_timer_id));
				}
			}, uint64(eConst::pipe_beat_heart_check_timer_id),
			uint64(eConst::pipe_beat_heart_check_timer_time));
		}

		void CPipeSession::sendBeatHeart() {
			heart_beat beatHeart;

			// build heat_beat message.
			std::vector<char> buffer;
			buffer.resize(sizeof(anet::tcp::SCommonHead) + sizeof(uint16) + sizeof(heart_beat));

			char* pBuff = &buffer[0];
			anet::tcp::SCommonHead* pHead = (anet::tcp::SCommonHead*)pBuff;
			pHead->len = decltype(pHead->len)(htonl(sizeof(uint16) + sizeof(heart_beat)));

			// message id
			*(uint16*)(pBuff + sizeof(anet::tcp::SCommonHead)) = htons(uint16(pipe_message_beat_heart_id));
			memcpy(pBuff + sizeof(anet::tcp::SCommonHead) + sizeof(uint16), &beatHeart, sizeof(beatHeart));

			// send to peer server.
			m_session->Send(pBuff, int(buffer.size()));
		}

		void CPipeSession::OnConnected() {
			m_state = ePipeState::pipe_session_state_net_connected;
			m_lastBeatHeart = GetNowMSTime();

			// for connector mode, send connect req.
			if (m_mode == ePipeMode::pipe_mode_connector) {
				this->sendConnectReq();
			} else {
				this->checkStatus();
			}
		}

		void CPipeSession::checkStatus() {
			m_timer.add_once_timer([this](void* pData) {
				if (this->m_state == ePipeState::pipe_session_state_net_connected) {
					gLog->Crit("find %s:%d client linger so log", this->GetRemotePipeName().c_str(), this->GetRemotePipeId());
					this->Close();
                }
			}, uint64(eConst::pipe_check_state_timer_id),
			uint64(eConst::pipe_check_state_timer_time));
		}

		void CPipeSession::OnTerminate() {
			// set pipe disconnected state.
			m_state = ePipeState::pipe_session_state_pipe_disconnected;

			// call OnTerminate().
			CPipeModule::instance().GetReporter()->OnReport(false, this);

			// remove entity name.
			CPipeModule::instance().RemoveService(m_remoteName+":"+std::to_string(m_remoteId));
		}

		void CPipeSession::OnRelease() {
			// please see OnCreate(), how to create and how to destroy it.
			delete this;
		}

		void CPipeSession::SetId(unsigned int id) {
			m_id = id;
		}

		unsigned int CPipeSession::GetId() const {
			return m_id;
		}

		void CPipeSession::sendConnectAck() {
			connect_ack ack;

			// build connect_ack message.
			std::vector<char> buffer;
			buffer.resize(sizeof(anet::tcp::SCommonHead) + sizeof(uint16) + sizeof(connect_ack));
			char* pBuff = &buffer[0];
			anet::tcp::SCommonHead* pHead = (anet::tcp::SCommonHead*)pBuff;
			pHead->len = (decltype(pHead->len))htonl(sizeof(uint16) + sizeof(connect_ack));

			// message id.
			*(uint16*)(pBuff + sizeof(anet::tcp::SCommonHead)) = htons(uint16(ePipeMessageId::pipe_message_connect_ack_id));
			memcpy(pBuff + sizeof(anet::tcp::SCommonHead) + sizeof(uint16), &ack, sizeof(ack));

			// send to the peer node.
			m_session->Send(pBuff, int(buffer.size()));
		}

		void CPipeSession::sendConnectReq() {
			connect_req req;
			// id
			req.remote_id = CPipeModule::instance().GetLocalId();
			// name
			safeCopy(req.remote_name, CPipeModule::instance().GetLocalName());
			// token.
			safeCopy(req.token, CPipeModule::instance().GetPipeConfig()->GetNodeToken(req.remote_name));
			
			// build connect_req message.
			std::vector<char> buffer;
			buffer.resize(sizeof(anet::tcp::SCommonHead) + sizeof(uint16) + sizeof(connect_req));
			char* pBuff = &buffer[0];
			anet::tcp::SCommonHead* pHead = (anet::tcp::SCommonHead*)pBuff;
			pHead->len = (decltype(pHead->len))htonl(sizeof(uint16) + sizeof(connect_req));

			*(uint16*)(pBuff + sizeof(anet::tcp::SCommonHead)) = htons(uint16(ePipeMessageId::pipe_message_connect_req_id));
			memcpy(pBuff + sizeof(anet::tcp::SCommonHead) + sizeof(uint16), &req, sizeof(req));
			
			// send to peer.
			m_session->Send(pBuff, int(buffer.size()));
		}
	}
}

