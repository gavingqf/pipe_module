#pragma once
/*
 * pipe base type definition.
 */

namespace anet {
	namespace pipe {
		// int8
		typedef char int8;

		// uint8
		typedef unsigned char uint8;

        // int16
		typedef short int16;

		// uint16
		typedef unsigned short uint16;

        // int32
		typedef int int32;

		// uint32
		typedef unsigned int uint32;

        // int64
		typedef long long int64;

		// uint64
		typedef unsigned long long uint64;
	}
}
